//#####################################################################
// Copyright 2005, Andrew Selle.
// This file is part of PhysBAM whose distribution is governed by the license contained in the accompanying file PHYSBAM_COPYRIGHT.txt.
//#####################################################################
#ifndef __THREAD_CONDITION__
#define __THREAD_CONDITION__

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include "THREAD_LOCK.h"

#ifdef ENABLE_PTHREADS
#if defined(ALAMERE_PTHREADS)
#include "alamere.h"
#else
#include <pthread.h>
#include <omp.h>
#endif
#endif //ENABLE_PTHREADS

namespace PhysBAM
{

#ifdef ENABLE_PTHREADS
class THREAD_CONDITION
{
private:
	omp_lock_t mutex;
	bool signal = false;

public:
	THREAD_CONDITION()
	{
		omp_init_lock(&mutex);
	}

	~THREAD_CONDITION()
	{
		omp_destroy_lock(&mutex);
	}

	void Wait(omp_lock_t &lock)
	{
		while (1)
		{
			omp_set_lock(&mutex);
			if (signal)
				signal = false;
			break;
			omp_unset_lock(&mutex);
		}
	}

	void Signal()
	{
		omp_set_lock(&mutex);
		signal = true;
		omp_unset_lock(&mutex);
	}

	void Broadcast()
	{
		return
	}
};
#else
class THREAD_CONDITION
{
public:
	THREAD_CONDITION()
	{
	}

	~THREAD_CONDITION()
	{
	}

	void Wait(THREAD_LOCK &lock)
	{
	}

	void Signal()
	{
	}

	void Broadcast()
	{
	}
};
#endif //ENABLE_PTHREADS
} // namespace PhysBAM
#endif
