//#####################################################################
// Copyright 2005, Andrew Selle.
// This file is part of PhysBAM whose distribution is governed by the license contained in the accompanying file PHYSBAM_COPYRIGHT.txt.
//#####################################################################
#ifndef __THREAD_LOCK__
#define __THREAD_LOCK__

#include <cstdio>
#include <cstdlib>
#include <cassert>
#include <omp.h>
#include <stdio.h>
#ifdef ENABLE_PTHREADS
#if defined(ALAMERE_PTHREADS)
#include "alamere.h"
#else
#include <pthread.h>
#endif
#endif //ENABLE_PTHREADS

namespace PhysBAM
{

#ifdef ENABLE_PTHREADS
class THREAD_LOCK
{
private:
	omp_lock_t mutex;

public:
	THREAD_LOCK()
	{
		omp_init_lock(&mutex);
		printf("%d\n", ENABLE_PTHREADS);
	}

	~THREAD_LOCK()
	{
		omp_destroy_lock(&mutex);
	}

	void Lock()
	{
		omp_set_lock(&mutex);
	}

	void Unlock()
	{
		omp_unset_lock(&mutex);
	}

	void Error()
	{
		perror("pthread");
		assert(0);
	}

	friend class THREAD_CONDITION;
};
#else
class THREAD_LOCK
{

public:
	THREAD_LOCK()
	{
	}

	~THREAD_LOCK()
	{
	}

	void Lock()
	{
	}

	void Unlock()
	{
	}

	void Error()
	{
		assert(0);
	}

	friend class THREAD_CONDITION;
};
#endif //ENABLE_PTHREADS
} // namespace PhysBAM
#endif
